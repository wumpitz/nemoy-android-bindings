package de.wumpitz.nemoy.bindings;

import android.os.Bundle;
import android.support.test.InstrumentationRegistry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
//import okhttp3.Response;
//import okhttp3.OkHttpClient;
//import okhttp3.RequestBody;
//import okhttp3.Request;
import retrofit2.*;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.Request;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import de.wumpitz.nemoy.bindings.models.Outgoing;
import de.wumpitz.nemoy.bindings.models.OutgoingsPager;
import de.wumpitz.nemoy.bindings.models.Pager;
import retrofit2.Call;
import retrofit2.converter.gson.GsonConverterFactory;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertThat;

/**
 * These are functional tests that make actual requests to the Spotify Web API endpoints and
 * compare raw JSON responses with the ones received with the interface crated by this library.
 * They require an access token to run, which is currently pretty manual and annoying but hopefully
 * we'll solve that in the future.
 *
 * Running the tests:
 * ./gradlew :spotify-api:connectedAndroidTest -Pandroid.testInstrumentationRunnerArguments.access_token=valid_access_token
 */
public class NemoyServiceAndroidTest {

    private OkHttpClient mClient = new OkHttpClient();
    private NemoyService mService;

    @Before
    public void setUp() throws Exception {
        NemoyApi nemoyApi = new NemoyApi();
        mService = nemoyApi.getService();
    }

    @Test
    public void getOutgoings() throws Exception {

        Request request = new Request.Builder()
                .get()
                .url("http://10.0.2.2:5000/outgoings")
                .build();

        Response response = mClient.newCall(request).execute();

        Call<Pager<Outgoing>> payload = mService.getOutgoings();
        retrofit2.Response<Pager<Outgoing>> pager = payload.execute();

        assertEquals(200, response.code());
    }

    private static class JsonEquals extends BaseMatcher<Object> {

        private static Gson sGson = new GsonBuilder().create();
        private final JsonElement mExpectedJsonElement;

        public JsonEquals(String expected) {
            mExpectedJsonElement = sGson.fromJson(expected, JsonElement.class);
        }

        @Override
        public boolean matches(Object actualRaw) {
            JsonElement actualJsonElement = sGson.toJsonTree(actualRaw);
            String withoutNulls = sGson.toJson(mExpectedJsonElement);
            JsonElement expectedJsonElementWithoutNulls = sGson.fromJson(withoutNulls, JsonElement.class);
            return expectedJsonElementWithoutNulls.equals(actualJsonElement);
        }

        @Override
        public void describeTo(Description description) {
            description.appendText(mExpectedJsonElement.toString());
        }

        static JsonEquals jsonEquals(String expected) {
            return new JsonEquals(expected);
        }
    }
}
