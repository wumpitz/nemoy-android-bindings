package de.wumpitz.nemoy.bindings;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Creates and configures a REST adapter for Spotify Web API.
 *
 * Basic usage:
 * NemoyApi wrapper = new NemoyApi();
 *
 * Setting access token is optional for certain endpoints
 * so if you know you'll only use the ones that don't require authorisation
 * you can skip this step:
 * wrapper.setAccessToken(authenticationResponse.getAccessToken());
 *
 * NemoyService spotify = wrapper.getService();
 *
 * Album album = spotify.getAlbum("2dIGnmEIy1WZIcZCFSj6i8");
 */
public class NemoyApi {

    /**
     * Main Nemoy API endpoint
     */
    public String NEMOY_API_ENDPOINT = "http://10.0.2.2:5000";

    private NemoyService mNemoyService;

    /**
     * Create instance of NemoyApi
     */
    public NemoyApi() {
        mNemoyService = init();
    }

    public NemoyApi(String endpoint) {
        this.NEMOY_API_ENDPOINT = endpoint;
        mNemoyService = init();
    }

    private NemoyService init() {
        Gson gson = new GsonBuilder()
                .setDateFormat("dd.MM.yyyy")
                .create();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(NEMOY_API_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

         return retrofit.create(NemoyService.class);
    }

    public void setEndpoint(String endpoint) {
        this.NEMOY_API_ENDPOINT = endpoint;
        mNemoyService = init();
    }

    /**
     * @return The NemoyApi instance
     */
    public NemoyService getService() {
        return mNemoyService;
    }
}
