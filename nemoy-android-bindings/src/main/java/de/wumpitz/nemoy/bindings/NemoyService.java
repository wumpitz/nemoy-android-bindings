package de.wumpitz.nemoy.bindings;

import de.wumpitz.nemoy.bindings.models.Outgoing;
import de.wumpitz.nemoy.bindings.models.Pager;
import retrofit2.http.*;
import retrofit2.Call;

public interface NemoyService {

    @GET("/outgoings")
    Call<Pager<Outgoing>> getOutgoings(@Query("sort") String order, @Query("where") String where);

    @GET
    Call<Pager<Outgoing>> previousPage(@Url String url);

    @GET
    Call<Pager<Outgoing>> nextPage(@Url String url);

    @POST("/outgoings")
    Call<Outgoing> createOutgoing(@Body Outgoing outgoing);

    @DELETE("/outgoings/{id}")
    Call<Outgoing> deleteOutgoing(@Path("id") String outgoingId, @Header("If-Match") String etag);
}
