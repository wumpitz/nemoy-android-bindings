package de.wumpitz.nemoy.bindings.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;
import java.util.List;

/**
 * Outgoing object model
 */
public class Outgoing implements Parcelable {
    public String _id;
    public String _etag;
    public Float amount;
    public List<String> tags;
    public Date date_spent;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this._id);
        dest.writeString(this._etag);
        dest.writeFloat(this.amount);
        dest.writeStringList(this.tags);
        dest.writeValue(this.date_spent);
    }

    public Outgoing() {
    }

    protected Outgoing(Parcel in) {
        this._id = in.readString();
        this._etag = in.readString();
        this.amount = in.readFloat();
        this.tags = in.createStringArrayList();
        this.date_spent = (Date) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<Outgoing> CREATOR = new Creator<Outgoing>() {
        public Outgoing createFromParcel(Parcel source) {
            return new Outgoing(source);
        }

        public Outgoing[] newArray(int size) {
            return new Outgoing[size];
        }
    };
}