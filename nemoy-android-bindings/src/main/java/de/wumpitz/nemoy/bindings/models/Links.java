package de.wumpitz.nemoy.bindings.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Meta object model
 */
public class Links implements Parcelable {
    public Link last;
    public Link next;
    public Link parent;
    public Link self;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.last, 0);
        dest.writeParcelable(this.next, 0);
        dest.writeParcelable(this.parent, 0);
        dest.writeParcelable(this.self, 0);
    }

    public Links() {
    }

    protected Links(Parcel in) {
        this.last = in.readParcelable(Link.class.getClassLoader());
        this.next = in.readParcelable(Link.class.getClassLoader());
        this.parent = in.readParcelable(Link.class.getClassLoader());
        this.self = in.readParcelable(Link.class.getClassLoader());
    }

    public static final Creator<Links> CREATOR = new Creator<Links>() {
        public Links createFromParcel(Parcel source) {
            return new Links(source);
        }

        public Links[] newArray(int size) {
            return new Links[size];
        }
    };
}