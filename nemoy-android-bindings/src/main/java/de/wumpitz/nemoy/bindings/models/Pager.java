package de.wumpitz.nemoy.bindings.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * <a href="https://developer.spotify.com/web-api/object-model/#paging-object">Paging object model</a>
 *
 * @param <T> expected object that is paged
 */
public class Pager<T> implements Parcelable  {
    public List<T> _items;
    public PagerMeta _meta;
    public Links _links;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(_items);
        dest.writeParcelable(this._meta, 0);
        dest.writeParcelable(this._links, 0);
    }

    public Pager() {
    }

    protected Pager(Parcel in) {
        this._items = in.readArrayList(ArrayList.class.getClassLoader());
        this._meta = in.readParcelable(PagerMeta.class.getClassLoader());
        this._links = in.readParcelable(Links.class.getClassLoader());

    }

    public static final Parcelable.Creator<Pager> CREATOR = new Parcelable.Creator<Pager>() {
        public Pager createFromParcel(Parcel source) {
            return new Pager(source);
        }

        public Pager[] newArray(int size) {
            return new Pager[size];
        }
    };

}
