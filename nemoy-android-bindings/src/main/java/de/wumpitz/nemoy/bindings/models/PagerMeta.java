package de.wumpitz.nemoy.bindings.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Meta object model
 */
public class PagerMeta implements Parcelable {
    public Integer max_results;
    public Integer page;
    public Integer total;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.max_results);
        dest.writeInt(this.page);
        dest.writeInt(this.total);
    }

    public PagerMeta() {
    }

    protected PagerMeta(Parcel in) {
        this.max_results = in.readInt();
        this.page = in.readInt();
        this.total = in.readInt();
    }

    public static final Creator<PagerMeta> CREATOR = new Creator<PagerMeta>() {
        public PagerMeta createFromParcel(Parcel source) {
            return new PagerMeta(source);
        }

        public PagerMeta[] newArray(int size) {
            return new PagerMeta[size];
        }
    };
}