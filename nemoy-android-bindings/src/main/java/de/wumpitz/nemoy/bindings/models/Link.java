package de.wumpitz.nemoy.bindings.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Meta object model
 */
public class Link implements Parcelable {
    public String href;
    public String title;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.href);
        dest.writeString(this.title);
    }

    public Link() {
    }

    protected Link(Parcel in) {
        this.href = in.readString();
        this.title = in.readString();
    }

    public static final Creator<Link> CREATOR = new Creator<Link>() {
        public Link createFromParcel(Parcel source) {
            return new Link(source);
        }

        public Link[] newArray(int size) {
            return new Link[size];
        }
    };
}